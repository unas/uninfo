# README #

This project is for an E-Ink display with a touch screen overlay for the raspberry pi.

### Project contains ####

Python code to run screen and get information from network/internet
Gerber files for PCB to drive touch screen

### Hardware needed ###

Raspberry PI, any newer models will do that have 40 gpio pins. Project was created with Zero W.
PCB for driving screen and touch overlay.
	Optional, you can also use any other chip that can run the touch screen overlay.
	Gerber files and eagle project included.
	The E-Ink screen does not need to be connected through this PCB, can be connected directly to the Raspberry PI.
	Components, e.g. from https://lcsc.com
		SMD resistors, size 1206, 10k x4, 1k x2
		SMD PCF8591, AD-converter with I2C, used for touch overlay
		SMD Transistors, MMBT2222A x4, MMBT2907A x2
		Touch screen connector, e.g. https://lcsc.com/product-detail/FFC-FPC-Flat-Flexible-Connectors_Boom-Precision-Elec-FPC-1-0mmpitch-4P-On-contact_C52847.html
7.5" E-Ink display. 
	https://www.waveshare.com/wiki/7.5inch_e-Paper_HAT_(B)
7" touch screen overlay. Optional, but required for python code to fully work.
	https://www.adafruit.com/product/1676